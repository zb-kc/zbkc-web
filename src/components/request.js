import {get, post} from '@/until/request.js'

// 省
export function province (data) {
  return get('/zbkc/area/provinceCode')
}

// 市
export function city (obj) {
  return get('/zbkc/area/cityCode/' + obj)
}

// 区
export function area (obj) {
  return get('/zbkc/area/areaCode/' + obj)
}

// 街道
export function street (obj) {
  return get('/zbkc/area/streetCode/' + obj)
}

// 社区
export function committee (obj) {
  return get('/zbkc/area/committeeCode/' + obj)
}

// 详细地址
export function details (obj) {
  return get('/zbkc/area/details/' + obj)
}
