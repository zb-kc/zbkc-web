import Vue from 'vue'
import Router from 'vue-router'
import NProgress from 'nprogress'
import store from '@/store'
import 'nprogress/nprogress.css'

Vue.use(Router)
const router = new Router({
  linkExactActiveClass: 'act',
  mode: 'history',
  routes: [
    {
      path: '/',
      component: () => import('@/views/login/index.vue')
    },
    {
      path: '/login',
      component: () => import('@/views/login/index.vue')
    },
    {
      path: '/index',
      name: 'index',
      meta: [ { name: '资产登记' } ],
      redirect: '/index/estate',
      component: () => import('@/components/template/index.vue'),
      children: [
        // 资产登记
        {
          name: 'estate',
          path: '/estate',
          meta: [ { name: '资产登记' } ],
          component: () => import('@/views/estate')
        },
        {
          name: 'estateMainPark',
          path: '/estate/mainPark',
          meta: [ { name: '资产登记' } ],
          component: () => import('@/views/estate/mainPark')
        },
        // 资产登记-商业
        {
          name: 'assetBusiness',
          path: '/business',
          meta: {title: '商业', require: true},
          component: () => import('@/views/business')
        },
        {
          name: 'businessMainPark',
          path: '/business/mainPark',
          meta: [ { name: '商业' } ],
          component: () => import('@/views/business/mainPark')
        },
        // 资产登记-孵化园区
        {
          name: 'assetIncubationPark',
          path: '/incubationPark',
          meta: {title: '孵化园区', require: true},
          component: () => import('@/views/incubationPark')
        },
        {
          name: 'incubationParkMain',
          path: '/incubationPark/mainPark',
          meta: [ { name: '孵化园区' } ],
          component: () => import('@/views/incubationPark/mainPark')
        },
        // 资产登记-行政办公
        {
          name: 'assetOffice',
          path: '/office',
          meta: {title: '行政办公', require: true},
          component: () => import('@/views/office')
        },
        {
          name: 'officeMain',
          path: '/office/mainPark',
          meta: [ { name: '行政办公' } ],
          component: () => import('@/views/office/mainPark')
        },
        // 资产登记-公共服务设施
        {
          name: 'assetPublicFacility',
          path: '/publicFacility',
          meta: {title: '公共服务设施', require: true},
          component: () => import('@/views/publicFacility')
        },
        {
          name: 'officeMain',
          path: '/publicFacility/mainPark',
          meta: [ { name: '公共服务设施' } ],
          component: () => import('@/views/publicFacility/mainPark')
        },
        // 资产登记-住宅
        {
          name: 'assetResidential',
          path: '/residential',
          meta: {title: '住宅', require: true},
          component: () => import('@/views/residential')
        },
        {
          name: 'officeMain',
          path: '/residential/mainPark',
          meta: [ { name: '住宅' } ],
          component: () => import('@/views/residential/mainPark')
        },
        // 资产登记-租赁社会物业
        {
          path: 'leaseSocialProperty',
          name: 'leaseSocialProperty',
          meta: {title: '租赁社会物业', require: true},
          component: () => import('@/views/assetsRegistration/leaseSocialProperty/index.vue')
        },
        // 资产管理
        {
          path: 'assetsDisposal',
          name: 'assetsDisposal',
          meta: {title: '资产处置列表', require: true},
          component: () => import('@/views/system/assetsManagement/assetsDisposal/index.vue')
        },
        {
          path: 'assetsEntry',
          name: 'assetsEntry',
          meta: {title: '资产入账列表', require: true},
          component: () => import('@/views/system/assetsManagement/assetsEntry/index.vue')
        },
        {
          path: 'propertyRegistration',
          name: 'propertyRegistration',
          meta: {title: '产权登记列表', require: true},
          component: () => import('@/views/system/assetsManagement/propertyRegistration/index.vue')
        },
        {
          path: 'leaseDetails',
          name: 'leaseDetails',
          meta: {title: '租赁明细', require: true},
          component: () => import('@/views/system/normalReport/leaseDetails/index.vue')
        },
        // 业务办理-代办业务
        {
          path: 'shouldHandleBusiness',
          name: 'shouldHandleBusiness',
          meta: {title: '待办业务', require: true},
          component: () => import('@/views/system/businessProcessing/shouldHandleBusiness/index.vue'),
          children: [
            {
              path: 'zerothStage',
              name: 'zerothStage',
              meta: {title: '用房移交申请办理-阶段0'},
              component: () => import('@/views/system/businessProcessing/dialog/transferRoomApply/zerothStage/index.vue')
            },
            {
              path: 'firstStage',
              name: 'firstStage',
              meta: {title: '用房移交申请办理-阶段1'},
              component: () => import('@/views/system/businessProcessing/dialog/transferRoomApply/firstStage/index.vue')
            },
            {
              path: 'secondStage',
              name: 'secondStage',
              meta: {title: '用房移交申请办理-阶段2'},
              component: () => import('@/views/system/businessProcessing/dialog/transferRoomApply/secondStage/index.vue')
            },
            {
              path: 'thirdStage',
              name: 'thirdStage',
              meta: {title: '用房移交申请办理-阶段3'},
              component: () => import('@/views/system/businessProcessing/dialog/transferRoomApply/thirdStage/index.vue')
            },
            {
              path: 'contractSignApply',
              name: 'contractSignApplyx',
              meta: {title: '合同签署申请办理'},
              component: () => import('@/views/system/businessProcessing/dialog/contractSignApply.vue'),
              children: [
                {
                  path: 'lastContract',
                  name: 'lastContractx',
                  meta: {title: '原合同信息'},
                  component: () => import('@/views/system/businessProcessing/secondPage/lastContract/index.vue'),
                  children: [
                    {
                      path: 'selectedProperty',
                      name: 'selectedProperty_x',
                      meta: {title: '已选物业列表'},
                      component: () => import('@/views/system/businessProcessing/secondPage/selectedProperty/index.vue')
                    }
                  ]
                },
                {
                  path: 'selectedProperty',
                  name: 'selectedPropertyx',
                  meta: {title: '已选物业列表'},
                  component: () => import('@/views/system/businessProcessing/secondPage/selectedProperty/index.vue')
                }
              ]
            },
            {
              path: 'propertyAssetsApply',
              name: 'propertyAssetsApplyx',
              meta: {title: '物业资产申报办理'},
              component: () => import('@/views/system/businessProcessing/dialog/propertyAssetsApply.vue')
            }
          ]
        },
        // 业务办理-所有业务
        {
          path: 'allBusiness',
          name: 'allBusiness',
          meta: {title: '所有业务', require: true},
          component: () => import('@/views/system/businessProcessing/AllBusiness/index.vue'),
          children: [
            // {
            //   path: 'transferRoomApply',
            //   name: 'transferRoomApply',
            //   meta: {title: '用房移交申请办理'},
            //   component: () => import('@/views/system/businessProcessing/dialog/transferRoomApply.vue')
            // },
            {
              path: 'zerothStage',
              name: 'zerothStage',
              meta: {title: '用房移交申请办理-阶段0'},
              component: () => import('@/views/system/businessProcessing/dialog/transferRoomApply/zerothStage/index.vue')
            },
            {
              path: 'firstStage',
              name: 'firstStage',
              meta: {title: '用房移交申请办理-阶段1'},
              component: () => import('@/views/system/businessProcessing/dialog/transferRoomApply/firstStage/index.vue')
            },
            {
              path: 'secondStage',
              name: 'secondStage',
              meta: {title: '用房移交申请办理-阶段2'},
              component: () => import('@/views/system/businessProcessing/dialog/transferRoomApply/secondStage/index.vue')
            },
            {
              path: 'thirdStage',
              name: 'thirdStage',
              meta: {title: '用房移交申请办理-阶段3'},
              component: () => import('@/views/system/businessProcessing/dialog/transferRoomApply/thirdStage/index.vue')
            },
            {
              path: 'contractSignApply',
              name: 'contractSignApply',
              meta: {title: '合同签署申请办理'},
              component: () => import('@/views/system/businessProcessing/dialog/contractSignApply.vue'),
              children: [
                {
                  path: 'lastContract',
                  name: 'lastContract',
                  meta: {title: '原合同信息'},
                  component: () => import('@/views/system/businessProcessing/secondPage/lastContract/index.vue'),
                  children: [
                    {
                      path: 'selectedProperty',
                      name: 'selectedProperty_y',
                      meta: {title: '已选物业列表'},
                      component: () => import('@/views/system/businessProcessing/secondPage/selectedProperty/index.vue')
                    }
                  ]
                },
                {
                  path: 'selectedProperty',
                  name: 'selectedProperty',
                  meta: {title: '已选物业列表'},
                  component: () => import('@/views/system/businessProcessing/secondPage/selectedProperty/index.vue')
                }
              ]
            },
            {
              path: 'propertyAssetsApply',
              name: 'propertyAssetsApply',
              meta: {title: '物业资产申报办理'},
              component: () => import('@/views/system/businessProcessing/dialog/propertyAssetsApply.vue')
            }
          ]
        },

        // {
        //   path: 'transferRoomApply',
        //   name: 'transferRoomApply',
        //   meta: {title: '用房移交申请办理', require: true},
        //   component: () => import('@/views/system/businessProcessing/dialog/transferRoomApply.vue')
        // },
        // {
        //   path: 'contractSignApply',
        //   name: 'contractSignApply',
        //   meta: {title: '合同签署申请办理', require: true},
        //   component: () => import('@/views/system/businessProcessing/dialog/contractSignApply.vue')
        // },
        // {
        //   path: 'lastContract',
        //   name: 'lastContract',
        //   meta: {title: '原合同信息'},
        //   component: () => import('@/views/system/businessProcessing/secondPage/lastContract/index.vue'),
        // },
        // {
        //   path: 'propertyAssetsApply',
        //   name: 'propertyAssetsApply',
        //   meta: {title: '物业资产申报办理', require: true},
        //   component: () => import('@/views/system/businessProcessing/dialog/propertyAssetsApply.vue')
        // },

        {
          path: 'messageManage',
          name: 'messageManage',
          meta: {title: '短信管理', require: true},
          component: () => import('@/views/system/businessProcessing/messageManage/index.vue')
        },

        // 运营管理
        {
          path: 'billManagement',
          name: 'billManagement',
          meta: {title: '账单列表', require: true},
          component: () => import('@/views/system/operationsManagement/billManagement/index.vue')
        },
        {
          path: 'depositManagement',
          name: 'depositManagement',
          meta: {title: '押金列表', require: true},
          component: () => import('@/views/system/operationsManagement/depositManagement/index.vue')
        },
        {
          path: 'entrustManagement',
          name: 'entrustManagement',
          meta: {title: '委托运营列表', require: true},
          component: () => import('@/views/system/operationsManagement/entrustManagement/index.vue')
        },
        {
          path: 'authorization',
          name: 'authorization',
          meta: {title: '企业授权管理', require: true},
          component: () => import('@/views/system/operationsManagement/authorization/index.vue')
        },

        {
          path: 'usageRegistion',
          name: 'usageRegistion',
          meta: {title: '使用登记列表', require: true},
          component: () => import('@/views/system/operationsManagement/usageRegistion/index.vue')
        },

        {
          path: 'contractManagement',
          name: 'contractManagement',
          meta: {title: '合同管理', require: true},
          component: () => import('@/views/system/operationsManagement/contractManagement/index.vue')
        },
        {
          path: 'addContract',
          name: 'addContract',
          meta: [ { name: '合同登记' } ],
          component: () => import('@/views/system/operationsManagement/contractManagement/AddContractTemplate.vue')
        },
        {
          path: 'seeContract',
          name: 'seeContract',
          meta: [ { name: '合同查看' } ],
          component: () => import('@/views/system/operationsManagement/contractManagement/SeeContract.vue')
        },
        {
          path: 'editContract',
          name: 'editContract',
          meta: [ { name: '编辑合同' } ],
          component: () => import('@/views/system/operationsManagement/contractManagement/editContract.vue')
        },
        {
          path: 'viewContract',
          name: 'viewContract',
          meta: [ { name: '查看合同' } ],
          component: () => import('@/views/system/operationsManagement/contractManagement/viewContract.vue')
        },

        // 常用报表
        {
          path: 'idleOfDetails',
          name: 'idleOfDetails',
          meta: {title: '空置明细', require: true},
          component: () => import('@/views/system/normalReport/idleOfDetails/index.vue')
        },
        {
          path: 'preExpirationContract',
          name: 'preExpirationContract',
          meta: {title: '预到期合同', require: true},
          component: () => import('@/views/system/normalReport/preExpirationContract/index.vue')
        },
        {
          path: 'reviewIndustryHousing',
          name: 'reviewIndustryHousing',
          meta: {title: '产业用房复核', require: true},
          component: () => import('@/views/system/normalReport/reviewIndustryHousing/index.vue')
        },
        // 预警提醒
        {
          path: 'contractExpiresWarning',
          name: 'contractExpiresWarning',
          meta: {title: '合同到期提醒', require: true},
          component: () => import('@/views/system/earlyWarningRemind/contractExpiresWarning/index.vue')
        },

        {
          // 子路由中默认显示的页面
          path: '',
          name: 'estate',
          component: () => import('@/views/estate')
        },

        {
          path: 'user',
          name: 'user',
          component: () => import('@/views/system/user.vue')
        },
        {
          path: 'role',
          name: 'role',
          component: () => import('@/views/system/role.vue')
        },
        {
          path: 'org',
          name: 'org',
          component: () => import('@/views/system/org.vue')
        },
        {
          path: 'menu',
          name: 'menu',
          component: () => import('@/views/system/menu.vue')
        },
        {
          path: 'parameter',
          name: 'parameter',
          component: () => import('@/views/system/parameter.vue')
        },
        {
          path: 'dic',
          name: 'dic',
          component: () => import('@/views/system/dic.vue')
        },
        {
          path: 'config',
          name: 'config',
          component: () => import('@/views/system/config.vue')
        }
        // {
        //   path: 'information',
        //   name: 'information',
        //   component: () => import('@/views/asset/information.vue')
        // }

      ]
    }
  ]
})

// 注册导航前置守卫
/*
    to:去哪
    from:从哪来
    to和from都有一个属性叫path，代表路径
    next:是一个函数，调用这个函数就代表放行，不调用就是不放行
        next不传参数就代表你以前想去哪还去哪，如果传参，就代表把你放行到你指定的路径
*/
router.beforeEach(async (to, from, next) => {
  NProgress.start()
  if (to.path === '/' || to.path === '/login') {
    NProgress.done()
    // 登录页 不需要判断
    next()
  } else {
    let tokenStr = sessionStorage.getItem('token')
    if (tokenStr) {
      NProgress.done()

      await store.dispatch('menu/getMenuData')// 加载菜单数据保存到store

      // 如果不需要，则直接跳转到对应路由
      next()
    } else {
      // 如果需要，则跳转到登录页
      Vue.prototype.$message.error('请先登录！')
      next('/login')
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  NProgress.done()
})

export default router
