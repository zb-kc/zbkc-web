import {
  menuList
} from '@/until/api.js'
//左侧菜单
export default {
  namespaced: true,
  state: {
    menuData: [],
    requestingMenuData: false
  },
  getters: {
    getMenuData(state) {
      return state.menuData
    },
    //根据路由路径获取该路由下的有权限的操作按钮 
    //['导出','新增']
    //   getBtnsByPath: (state) => {
    //     return async (path) => {
    //       //没有菜单数据时调接口获取
    //       if (state.menuData.length == 0 && !state.requestingMenuData) {
    //         state.requestingMenuData = true;
    //         await menuList(sessionStorage.getItem('userId')).then(res => {
    //           if (res.data.code === 0) {
    //             state.menuData = res.data.data;
    //           }
    //           state.requestingMenuData = false;
    //         })
    //       }

    //       let result = [];
    //       //根据路由查找节点
    //       let findNode = (node) => {
    //         let resultNode = null;
    //         if (node) {
    //           if (node.routerPath === path) {
    //             resultNode = node;
    //           } else if (node.sysMenusList && node.sysMenusList.length > 0) {
    //             for (let i = 0; i < node.sysMenusList.length; i++) {
    //               resultNode = findNode(node.sysMenusList[i]);
    //               if (resultNode) break;
    //             }
    //           }
    //         }
    //         return resultNode;
    //       }
    //       let resultNode;
    //       for (let i = 0; i < state.menuData.length; i++) {
    //         resultNode = findNode(state.menuData[i]);
    //         if (resultNode) break;
    //       }
    //       if (resultNode && resultNode.sysMenusList && resultNode.sysMenusList.length > 0) {
    //         result = resultNode.sysMenusList.map((v) => {
    //           return v.name
    //         });
    //       }
    //       return result;
    //     }
    //   }
    // },
    //根据路由路径获取该路由下的有权限的操作按钮 
    //['导出','新增']
    getBtnsByPath: (state) => {
      return (path) => {
        let result = [];
        //根据路由查找节点
        let findNode = (node) => {
          let resultNode = null;
          if (node) {
            if (node.routerPath === path) {
              resultNode = node;
            } else if (node.sysMenusList && node.sysMenusList.length > 0) {
              for (let i = 0; i < node.sysMenusList.length; i++) {
                resultNode = findNode(node.sysMenusList[i]);
                if (resultNode) break;
              }
            }
          }
          return resultNode;
        }
        let resultNode;
        for (let i = 0; i < state.menuData.length; i++) {
          resultNode = findNode(state.menuData[i]);
          if (resultNode) break;
        }
        if (resultNode && resultNode.sysMenusList && resultNode.sysMenusList.length > 0) {
          result = resultNode.sysMenusList.map((v) => {
            return v.name
          });
        }
        return result;
      }
    }
  },
  mutations: {
    setMenuData(state, payload) {
      state.menuData = payload
    }
  },
  actions: {
    getMenuData: async ({
      state,
      commit
    }) => {
      // 这里是为了防止重复获取
      if (state.menuData.length) return
      let list = []; // = await api.getPermissionList()
      await menuList(sessionStorage.getItem('userId')).then(res => {
        if (res.data.code === 0) {
          list = res.data.data;
        }
      })
      commit('setMenuData', list)
    },
    //清空菜单
    clearMenuData: ({
      state,
      commit
    }) => {
      commit('setMenuData', [])
    }
  }
}
