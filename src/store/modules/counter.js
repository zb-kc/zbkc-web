export default {
  namespaced: true, // 命名空间
  state: {
    count: 0
  },
  getters: {
    getCount (state) {
      return state.count
    }
  },
  mutations: {
    add (state, payload) {
      state.count += payload
    },
    minus (state, payload) {
      state.count -= payload
    }
  },
  actions: {
    asyncMinus (context, payload) {
      setTimeout(() => {
        // 异步操作，假设这里在发送ajax请求
        context.commit('minus', payload)
      }, 2000)
    }
  }
}
