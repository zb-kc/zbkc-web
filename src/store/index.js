import Vue from 'vue'
import Vuex from 'vuex'

// 导入模块
import counter from './modules/counter'
import user from './modules/user'
import menu from './modules/menu'
import global from './modules/global'

Vue.use(Vuex)

export default new Vuex.Store({
  // 对模块进行注册
  modules: {
    counter,
    user,
    menu,
    global
  }
})
