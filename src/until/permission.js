import Vue from 'vue'
import router from '@/router/index.js'
import store from '@/store'

// 注册自定义指令'v-permission'
// 使用：<el-button v-permission="{action:'add',effect:'disabled'}">新增</el-button>
// 页面内部跳转了路由需要控制按钮权限时，需要传入父级页面的路由路径path<el-button v-permission="{action:'add',effect:'disabled',path:'/index/contractManagement'}">新增</el-button>
Vue.directive('permission', {
  inserted: async function (el, binding) {
    // 当指令插入元素调用时，可以传递两个值：el:指令所在的元素；binding:一个对象，包含当前元素的propety属性
    // console.log(el)
    // console.log(binding)
    const action = binding.value.action
    // 此按钮指令中的值
    // console.log(router.currentRoute)
    // const currentPer = router.currentRoute.meta.permission;
    let path = router.currentRoute.path
    if (binding.value.path && binding.value.path != '') {
      path = binding.value.path
    }
    // console.log(path)
    if (path) {
      // store.getters['menu/getBtnsByPath'](path).then(res => {
      //   const currentPer = res;
      //   //const currentPer =
      //   //此变量为路由动态添加后的权限的值
      //   //console.log(currentPer)
      //   if (currentPer) {
      //     if (currentPer.indexOf(action) === -1) {
      //       // 不具备权限，按钮应该为禁用或者不存在
      //       const type = binding.value.effect
      //       if (type === 'disabled') {
      //         el.disabled = true //激活禁用
      //         el.classList.add('is-disabled')
      //       } else if (type === 'remove') {
      //         el.parentNode.removeChild(el) //删除
      //       } else {
      //         el.disabled = true //激活禁用
      //         el.classList.add('is-disabled')
      //       }
      //     }
      //   }
      // });

      const currentPer = store.getters['menu/getBtnsByPath'](path)
      // const currentPer =
      // 此变量为路由动态添加后的权限的值
      // console.log(currentPer)
      if (currentPer) {
        if (currentPer.indexOf(action) === -1) {
          // 不具备权限，按钮应该为禁用或者不存在
          const type = binding.value.effect
          if (type === 'disabled') {
            el.disabled = true // 激活禁用
            el.classList.add('is-disabled')
          } else if (type === 'remove') {
            el.parentNode.removeChild(el) // 删除
          } else {
            el.disabled = true // 激活禁用
            el.classList.add('is-disabled')
          }
        }
      }
    }
  }

})
