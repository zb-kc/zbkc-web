import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router/index'
import store from './store'
import permission from './until/permission'
// import axios from 'axios'
import css from '../src/until/css.css'
// 导入全局基础样式
import './assets/base.css'
import './assets/common.css'
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'
import VueAMap from 'vue-amap'
import VModal from 'vue-js-modal'

Vue.use(VueAMap)
Vue.use(VModal, {
  dialog: true,
  dynamic: true
})
VueAMap.initAMapApiLoader({
  key: '0e6a8953b9ecae939e2bf4bff45c2be4',
  plugin: [
    'AMap.Autocomplete', // 输入提示插件
    'AMap.PlaceSearch', // POI搜索插件
    'AMap.Scale', // 右下角缩略图插件 比例尺
    'AMap.OverView', // 地图鹰眼插件
    'AMap.ToolBar', // 地图工具条
    'AMap.MapType', // 类别切换控件，实现默认图层与卫星图、实施交通图层之间切换的控制
    'AMap.PolyEditor', // 编辑 折线多，边形
    'AMap.CircleEditor', // 圆形编辑器插件
    'AMap.Geolocation' // 定位控件，用来获取和展示用户主机所在的经纬度位置
  ],
  v: '1.4.4',
  uiVersion: '1.0'
})

Vue.use(Viewer)
Vue.use(css)
Viewer.setDefaults({
  Options: { 'inline': true, 'button': true, 'navbar': true, 'title': true, 'toolbar': true, 'tooltip': true, 'movable': true, 'zoomable': true, 'rotatable': true, 'scalable': true, 'transition': true, 'fullscreen': true, 'keyboard': true, 'url': 'data-source' },
  zIndexInline: 2050
})

Vue.use(ElementUI)
Vue.config.productionTip = false
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
