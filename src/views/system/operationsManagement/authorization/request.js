import {
  get,
  post,
  request
} from '@/until/request'

export function exportData(obj) {
  return request('post', '/zbkc/yyCompanyAuthorization/export', obj, null, 'blob')
}
