import { propertyShow, downLoadPaymentNotice, downLoadContactTemplate, downloadFileOne, downloadBatchByFile } from '@/until/api.js'
// downloadBatchByFile
export default {
  data () {
    return {
      circulationVisible: false,
      approvalFormVIsible: false,
      editVisible: false,
      sendMsgVisible: false,
      fillFillVisible: false,
      checkProcessVisible: false,
      selectAddressVisible: false,
      hasSelectAddressVisible: false,
      iframeFileSeeVisible: false,
      contractButtonVisible: true,
      selectPropertyID: [],
      discountArr: [
        {
          value: '1',
          label: '全价'
        },
        {
          value: '2',
          label: '30%'
        },
        {
          value: '3',
          label: '50%'
        },
        {
          value: '4',
          label: '70%'
        },
        {
          value: '5',
          label: '90%'
        }
      ],
      freeRentArr: [
        {
          value: '0',
          label: '无'
        },
        {
          value: '1',
          label: '十五日'
        },
        {
          value: '2',
          label: '一个月'
        },
        {
          value: '3',
          label: '两个月'
        },
        {
          value: '4',
          label: '三个月'
        },
        {
          value: '5',
          label: '四个月'
        }
      ],
      selectStatus: {
        0: '待审核',
        1: '材料审核通过',
        2: '未通过审核'
      },
      selectFreeRent: {
        0: '无',
        1: '十五日',
        2: '一个月',
        3: '两个月',
        4: '三个月',
        5: '四个月'
      },
      selectDiscount: {
        1: '全价',
        2: '30%',
        3: '50%',
        4: '70%',
        5: '90%'
      },
      selectEconomic: {
        1: '承诺',
        2: '不承诺'
      },
      selsectSplitRent: {
        1: '是',
        2: '否'
      },
      selectLessee: {
        1: '企业',
        2: '其他'
      },
      selectWebSignType: {
        1: '新签',
        2: '续签'
      },
      selectLeaserType: {
        1: '社会统一信用代码',
        2: '身份证',
        3: '护照'
      },
      selsectClientType: {
        1: '身份证',
        2: '护照'
      },
      selectApplyType: {
        1: '法人申请',
        2: '委托代理人申请'
      },
      selectSignSource: {
        1: '现场申请',
        2: '线上申请'
      },
      rules: {
        leaseWyAddress: [{ required: true, message: '请选择出租物业地址', trigger: 'change' }],
        isEconomicCommitment: [{ required: true, message: '请选择是否经济贡献承诺', trigger: 'change' }],
        splitRent: [{ required: true, message: '请选择是否拆分租金单价', trigger: 'change' }],
        houseOwnership: [{ required: true, message: '请选择房屋权属情况', trigger: 'change' }],
        partAHouseCredentials: [{ required: true, message: '请选择甲方持有房屋证明文件', trigger: 'change' }],
        startRentDate: [{ type: 'date', required: true, message: '请选择租赁起始日期', trigger: 'change' }],
        endRentDate: [{ type: 'date', required: true, message: '请选择租赁截止日期', trigger: 'change' }],
        leaseUse: [{ required: true, message: '请选择房屋租赁用途', trigger: 'change' }],
        firstRentTime: [{ type: 'date', required: true, message: '请选择首期租金交付日期', trigger: 'change' }],
        economicCommitment: [{ required: true, message: '请输入经济贡献承诺', trigger: 'change' }],
        rentUnitPrice: [{ required: true, message: '请输入租金单价', trigger: 'change' }],
        firstRentMoney: [{ required: true, message: '请输入首期租金交付金额', trigger: 'change' }],
        depositAmount: [{ required: true, message: '请输入押金金额', trigger: 'change' }],
        waterCharge: [{ required: true, message: '请输入水费', trigger: 'change' }],
        electricCharge: [{ required: true, message: '请输入电费', trigger: 'change' }],
        gasCharge: [{ required: true, message: '请输入燃气费', trigger: 'change' }],
        propertyManagementCharge: [{ required: true, message: '请输入物业管理费', trigger: 'change' }]
      }
    }
  },
  methods: {
    // 获取上传附件传递的数据
    getUpdateData (data) {
      this.fileData['id'] = data.id
    },
    // 附件上传成功的数据回调
    changeWps (wpsList) {
      this.fileData = []
      Array.isArray(wpsList) && wpsList.forEach((item) => {
        let val = item.response.data
        this.fileData['fileName'] = val.fileName
        this.fileData['path'] = val.filePath
      })
    },
    buttonVisible (event) {
      if (event > 0) {
        this.contractButtonVisible = true
      } else {
        this.contractButtonVisible = false
      }
    },
    // 删除合同附件
    deleteFile () {
      this.$nextTick(() => {
        this.$refs.uploadFile.beforeRemove()
      })
    },
    // 下载附件
    downLoadFile (mark, row) {
      let actions = {
        'notice': () => {
          downLoadPaymentNotice(this.businessId).then(res => {
            if (res.data.code) {
              if (res.data.code !== 0) {
                this.$message.warning(res.data.errMsg)
              }
            } else {
              window.location.href = '/zbkc/ywBusiness/yw/downLoadPaymentNotice?businessId=' + this.businessId
            }
          })
        },
        'template': () => {
          downLoadContactTemplate(this.businessId).then(res => {
            if (res.data.code) {
              if (res.data.code !== 0) {
                this.$message.warning(res.data.errMsg)
              }
            } else {
              window.location.href = '/zbkc/ywBusiness/yw/downLoadContactTemplate?businessId=' + this.businessId
            }
          })
        },
        'contract': () => {
          downloadFileOne(this.businessId).then(res => {
            if (res.data.code) {
              if (res.data.code !== 0) {
                this.$message.warning(res.data.errMsg)
              }
            } else {
              window.location.href = '/zbkc/ywBusiness/yw/downloadFileOne?ywHandlerDetailsId=' + this.businessId
            }
          })
        },
        'apply': () => {
          downloadFileOne(row.id).then(res => {
            if (res.data.code) {
              if (res.data.code !== 0) {
                this.$message.warning(res.data.errMsg)
              }
            } else {
              window.location.href = '/zbkc/ywBusiness/yw/downloadFileOne?ywHandlerDetailsId=' + row.id
            }
          })
        }
      }
      actions[mark]()
    },

    // 申请材料的显示控制
    getApplyFile () {
      if (this.applyFileList) {
        let applyFile = this.applyFileList
        for (var i = 0; i < applyFile.length; i++) {
          if (this.dialogForm.legalApplication === '2') {
            if (applyFile[i].name === '法人授权委托书' || applyFile[i].name === '经办人身份证复印件') {
              applyFile.splice(i, 1)
            }
          }
          if (this.dialogForm.canModify) {
            if (this.dialogForm.isEconomicCommitment === '2') {
              if (applyFile[i].name === '有经济贡献率的企业的经济贡献承诺书') {
                applyFile.splice(i, 1)
              }
            }
          } else {
            if (this.dialogForm.isEconomicCommitment === '2') {
              if (applyFile[i].name === '有经济贡献率的企业的经济贡献承诺书') {
                applyFile.splice(i, 1)
              }
            }
          }
        }
        this.applyFileList = applyFile
      }
    },
    // 申请材料多选打包下载
    handleSelectionChange (val) {
      this.fileMultipleSelect = []
      for (let item of val) {
        this.fileMultipleSelect.push(item.id)
      }
      this.selectFileNum = this.fileMultipleSelect.length
    },
    // 附件打包下载
    packDownLoad () {
      downloadBatchByFile(this.fileMultipleSelect.toString()).then(res => {
        if (res.data.code) {
          if (res.data.code !== 0) {
            this.$message.warning(res.data.errMsg)
          }
        } else {
          window.location.href = '/zbkc/ywBusiness/yw/downloadBatchByFile?downloadFileIds=' + this.fileMultipleSelect.toString()
        }
      })
    },
    // 返回上一页
    backLastPage () {
      if (this.$route.params.mark === 'should') {
        this.$router.push({ name: 'shouldHandleBusiness', params: { activeName: 'second' } })
      } else {
        this.$router.push({ name: 'allBusiness', params: { activeName: 'second' } })
      }
    },

    // 选择物业资产的子组件数据修改
    operateSon (data) {
      if (data) {
        this.getDataList()
        this.getPropertyList()
      }
    },
    // 编辑拆分租金表格子组件数据修改
    editData (data) {
      this.calculateMonthRent()
      // this.getPropertyList()
    },

    // 原合同信息按钮
    getLastContract () {
      console.log(this.businessId, 'this.businessId')
      if (this.$route.path === '/index/allBusiness/contractSignApply') {
        this.$router.push(
          {
            name: 'lastContract',
            params: {
              businessId: this.businessId
            }
          }
        )
      } else {
        this.$router.push(
          {
            name: 'lastContractx',
            params: {
              businessId: this.businessId
            }
          }
        )
      }
    },
    // 获取拆分物业的列表
    getPropertyList () {
      this.selectPropertyID = []
      propertyShow(this.dialogForm.id).then(res => {
        this.rentTableData = res.data.data
        for (let item of this.rentTableData) {
          this.selectPropertyID.push(item.id)
        }
        if (this.rentTableData.length > 1) {
          this.calculateMonthRent()
        }
      })
    },
    // 计算拆分物业列表的月租金
    calculateMonthRent () {
      this.dialogForm.monthlyRent = 0
      this.dialogForm.buildArea = 0
      this.dialogForm.rentalArea = 0
      this.dialogForm.setArea = 0
      this.dialogForm.pubArea = 0
      for (let item of this.rentTableData) {
        if (item.monthlyRent) {
          this.dialogForm.monthlyRent += parseInt(item.monthlyRent)
        }
        this.dialogForm.buildArea += parseInt(item.buildArea)
        this.dialogForm.rentalArea += parseInt(item.buildArea)
        this.dialogForm.setArea += parseInt(item.buildArea)
        this.dialogForm.pubArea += parseInt(item.shareArea)
      }
    },
    // 不拆分租金单价时 月租金=租金单价*租赁面积
    changeMonthPrice () {
      if (this.dialogForm.splitRent === '2') {
        this.dialogForm.monthlyRent = ''
        for (let item of this.rentTableData) {
          this.dialogForm.monthlyRent = item.buildArea * this.dialogForm.rentUnitPrice
        }
      }
    },
    // 指导价*折扣价=租金单价
    changeUnitPrice () {
      let discount = ''
      if (this.dialogForm.discount) {
        discount = this.selectDiscount[this.dialogForm.discount].replace('%', '') / 100
      }
      if (this.dialogForm.guidePrice && discount) {
        this.dialogForm.rentUnitPrice = this.dialogForm.guidePrice * discount
      } else if (this.dialogForm.guidePrice) {
        this.dialogForm.rentUnitPrice = this.dialogForm.guidePrice
      } else {
        this.dialogForm.rentUnitPrice = ''
      }
      this.changeMonthPrice()
    },
    // 材料补正提交后刷新列表审核状态变为未审核
    reflashTableList () {
      this.getFileList()
    },
    // 流程提交变动后直接返回上一页
    backPage () {
      this.backLastPage()
    },
    // 时间戳转化成日期
    formatDate (dataStr) {
      var time = new Date(dataStr)
      function timeAdd0 (str) {
        if (str < 10) {
          str = '0' + str
        }
        return str
      }
      var y = time.getFullYear()
      var m = time.getMonth() + 1
      var d = time.getDate()
      var h = time.getHours()
      var mm = time.getMinutes()
      var s = time.getSeconds()
      return y + '-' + timeAdd0(m) + '-' + timeAdd0(d) + ' ' +
        timeAdd0(h) + ':' + timeAdd0(mm) + ':' + timeAdd0(s)
    },
    // 字节转化
    byteToChange (limit) {
      var size = ''
      if (limit < 0.1 * 1024) {
        size = Number(limit).toFixed(2) + 'B'
      } else if (limit < 0.1 * 1024 * 1024) {
        size = Number(limit / 1024).toFixed(2) + 'KB'
      } else if (limit < 0.1 * 1024 * 1024 * 1024) {
        size = Number(limit / (1024 * 1024)).toFixed(2) + 'MB'
      } else {
        size = Number(limit / (1024 * 1024 * 1024)).toFixed(2) + 'GB'
      }
      var sizeStr = size + ''
      var index = sizeStr.indexOf('.')
      var dou = sizeStr.substr(index + 1, 2)
      if (dou === '00') {
        return sizeStr.substring(0, index) + sizeStr.substr(index + 3, 2)
      }
      return size
    },
    // 展示弹框/跳转页面
    showDialog (mark, data) {
      const actions = {
        'info': () => {
          this.circulationVisible = true
          this.$nextTick(() => {
            this.$refs.CirculationInfoForm.title = '合同签署申请流转信息'
            this.$refs.CirculationInfoForm.businessId = this.businessId
            this.$refs.CirculationInfoForm.show()
          })
        },
        'approve': () => {
          this.approvalFormVIsible = true
          this.$nextTick(() => {
            this.$refs.approvalForm.businessId = this.businessId
            this.$refs.approvalForm.show()
          })
        },
        'edit': () => {
          this.editDialogData = data
          this.editVisible = true
          this.$nextTick(() => {
            this.$refs.EditDialog.show()
          })
        },
        'send': () => {
          this.sendMsgVisible = true
          this.$nextTick(() => {
            this.$refs.SendMsg.show()
          })
        },
        'fill': () => {
          this.fillFillVisible = true
          this.$nextTick(() => {
            this.$refs.FileFillUp.show()
          })
        },
        'address': () => {
          this.selectAddressVisible = true
          this.$nextTick(() => {
            this.$refs.SelectAddress.show()
          })
        },
        'hasaddress': () => {
          if (this.$route.path === '/index/allBusiness/contractSignApply') {
            this.$router.push(
              {
                name: 'selectedProperty',
                params: {
                  mark: this.$route.path,
                  contractId: this.dialogForm.id
                }
              }
            )
          } else {
            this.$router.push(
              {
                name: 'selectedPropertyx',
                params: {
                  contractId: this.dialogForm.id
                }
              }
            )
          }
        },
        'see': () => {
          let path = data.sysFilePathList[0].path
          const flieArr = path.split('.')
          let suffix = flieArr[flieArr.length - 1]
          path = path.slice(path.indexOf('/zbkc'))
          // || suffix === 'pdf'
          if (suffix === 'docx') {
            this.preViewType = suffix
            this.preViewSrc = path
            this.iframeFileSeeVisible = true
            this.$nextTick(() => {
              this.$refs.IframeFileSee.show()
            })
          } else {
            this.$message({
              type: 'warning',
              message: 'docx之外的文件请下载后查看'
            })
          }
        },
        'end': () => {
          this.checkTitle = '终止流程'
          this.checkLabel = '终止意见'
          this.checkProcessVisible = true
          this.$nextTick(() => {
            this.$refs.CheckProcess.show()
          })
        },
        'back': () => {
          this.checkTitle = '驳回'
          this.checkLabel = '驳回意见'
          this.checkProcessVisible = true
          this.$nextTick(() => {
            this.$refs.CheckProcess.show()
          })
        },
        'examine': () => {
          this.checkTitle = '审核通过'
          this.checkLabel = '审核意见'
          this.checkProcessVisible = true
          this.$nextTick(() => {
            this.$refs.CheckProcess.show()
          })
        }
      }
      actions[mark]()
    }
  }
}
