import {
  get,
  post,
  request
} from '@/until/request.js'

//下载租赁缴款通知单
export function downLoadPaymentNotice(obj) {
  return request('post', '/zbkc/yyContract/downLoadPaymentNotice', obj, null, 'blob')
}
//下载合同模板
export function downLoadContactTemplate(obj) {
  return request('post', '/zbkc/yyContract/downLoadContactTemplate', obj, null, 'blob')
}
