import {get, post, request} from '@/until/request'

export function allProRight (userId) {
  return post('/zbkc/wyAssetEntry/allProRight?userId=' + userId)
}
export function selectTag (obj) {
  return get('/zbkc/sysDataType/code/' + obj)
}
export function listId (obj) {
  return get('/zbkc/yyCommissioneOperation/row/' + obj)
}
export function list (obj) {
  return post('/zbkc/yyCommissioneOperation/list', obj)
}
export function add (obj) {
  return post('/zbkc/yyCommissioneOperation/addOperation', obj)
}
export function update (obj) {
  return post('/zbkc/yyCommissioneOperation/updOperation', obj)
}
//导出
export function exportData(obj) {
  return request('post', '/zbkc/yyCommissioneOperation/export', obj, null, 'blob')
}