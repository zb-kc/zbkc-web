import { get, post, request } from '@/until/request'
// 账单管理列表
export function getOrderList (obj) {
  return post('zbkc/yyBillManagement/homeList', obj)
}
// 查看账单
export function checkOrder (obj) {
  return get('zbkc/yyBillManagement/seeRow' + obj)
}

// 账单收款
export function rebackOrder (obj) {
  return post('zbkc/yyBillManagement/collecInit', obj)
}
// 账单收款 多户
export function rebackOrderMany (obj) {
  return get('zbkc/yyBillManagement/collection/many' + obj)
}
// 确认收款
export function collecMoney (obj) {
  return post('zbkc/yyBillManagement/collecMoney/save', obj)
}

// 确认收款 多户
export function collecMoneyMany (obj) {
  return post('zbkc/yyBillManagement/many/save', obj)
}

// 催缴记录
export function getCallRecord (obj) {
  return post('zbkc/yyBillManagement/callRecord', obj)
}

// 查看账单-保存
export function save (obj) {
  return post('zbkc/yyBillManagement/save', obj)
}
// 通过物业类型查询物业名称
export function getWyName (obj) {
  return get('zbkc/yyBillManagement/type/name' + obj)
}
// 获取每月缴费情况
export function getBillCount (obj) {
  return get('zbkc/yyBillManagement/pay' + obj)
}
// 获取所有短信模板名称
export function getMsgName () {
  return get('zbkc/yyBillManagement/template/name')
}
// 获取所有短信内容
export function getMsgContent (name) {
  return get('zbkc/yyBillManagement/word?name=' + name)
}
// 导出
export function exportData (obj) {
  return request('post', '/zbkc/yyBillManagement/list/export', obj, null, 'blob')
}
