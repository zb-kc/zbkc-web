import {
  get,
  post,
  request
} from '@/until/request'

export function allProRight (obj) {
  return post('/zbkc/wyAssetEntry/allProRight?userId=' + sessionStorage.getItem('userId'))
  // return post('/zbkc/wyBasicInfo/property/structure/all', obj)
}
// 获取所有空置物业
export function getEmptyList () {
  return get('/zbkc/wyBasicInfo/wyInfo/empty?userId=' + sessionStorage.getItem('userId'))
}
export function selectTag (obj) {
  return get('/zbkc/sysDataType/code/' + obj)
}
export function listId (obj) {
  return get('/zbkc/yyUseReg/row/' + obj)
}
export function list (obj) {
  return post('/zbkc/yyUseReg/list', obj)
}
export function addEntry (obj) {
  return post('/zbkc/yyUseReg/addUseReg', obj)
}
export function update (obj) {
  return post('/zbkc/yyUseReg/upd', obj)
}
// 物业详情
export function wyDetails (id) {
  return get('/zbkc/wyBasicInfo/details/' + id)
}
// 导出
export function exportData (obj) {
  return request('post', '/zbkc/yyUseReg/list/export', obj, null, 'blob')
}
// 停用
export function stop (id) {
  return get('/zbkc/yyUseReg/use/stop?status=2&id=' + id)
}
