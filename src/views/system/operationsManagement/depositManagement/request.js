import {
  get
} from '@/until/request.js'

export function selectTag(obj) {
  return get('/zbkc/sysDataType/code/' + obj)
}

export function details(obj) {
  return get('/zbkc/yyDeposit/seeRow/' + obj)
}