import {get, post} from '@/until/request'

export function allProRight () {
  return post('/zbkc/wyAssetEntry/allProRight?userId=' + sessionStorage.getItem('userId'))
}
export function selectTag (obj) {
  return get('/zbkc/sysDataType/code/' + obj)
}
export function listId (obj) {
  return get('/zbkc/wyAssetDisposal/row/' + obj)
}
export function list (obj) {
  return post('/zbkc/wyAssetDisposal/list', obj)
}
export function addEntry (obj) {
  return post('/zbkc/wyAssetDisposal/addDisposal', obj)
}
export function update (obj) {
  return post('/zbkc/wyAssetDisposal/upd', obj)
}
// 删除资产处置
export function delwyAssetDisposal (id) {
  return get('/zbkc/wyAssetDisposal/del/' + id)
}
