import {get, post} from '@/until/request'

export function allProRight (id) {
  return post('/zbkc/wyProRightReg/allProRight?userId=' + id)
}
export function selectTag (obj) {
  return get('/zbkc/sysDataType/code/' + obj)
}
export function Listid (obj) {
  return get('/zbkc/wyProRightReg/row/' + obj)
}
export function addEntry (obj) {
  return post('/zbkc/wyProRightReg/addProRight', obj)
}
export function update (obj) {
  return post('/zbkc/wyProRightReg/upd', obj)
}
export function list (obj) {
  return post('/zbkc/wyProRightReg/list', obj)
}
// 删除产权登记
export function delwyProRightReg (id) {
  return get('/zbkc/wyProRightReg/del/' + id)
}
// 地址查询
export function searchAddress (obj) {
  return get('/zbkc/area/details/' + obj)
}
