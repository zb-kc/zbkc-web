import {get, post} from '@/until/request'

export function allProRight () {
  return post('/zbkc/wyAssetEntry/allProRight?userId=' + sessionStorage.getItem('userId'))
}
export function selectTag (obj) {
  return get('/zbkc/sysDataType/code/' + obj)
}
export function listId (obj) {
  return get('/zbkc/wyAssetEntry/row/' + obj)
}
export function list (obj) {
  return post('/zbkc/wyAssetEntry/list', obj)
}
export function addEntry (obj) {
  return post('/zbkc/wyAssetEntry/addEntry', obj)
}
export function update (obj) {
  return post('/zbkc/wyAssetEntry/upd', obj)
}
// 删除资产入账
export function delwyAssetEntry (id) {
  return get('/zbkc/wyAssetEntry/del/' + id)
}
