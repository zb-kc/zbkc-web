import {get, post} from '../../../until/request'
// 获取标签
export function selectTag (obj) {
  return get('/zbkc/sysDataType/code/' + obj)
}

// 获取街道 南山区code(440305)
export function streetCode (obj) {
  return get('/zbkc/area/streetCode/' + obj)
}

// 获取社区
export function committeeCode (obj) {
  return get('/zbkc/area/committeeCode/' + obj)
}

// 获取物业结构
export function getStructure (obj) {
  return get('/zbkc/wyBasicInfo/property/structure' + obj)
}

export function details (id) {
  return get('/zbkc/wyBasicInfo/details/' + id)
}
// 获取物业编号
export function propertyCode () {
  return get('/zbkc/wyBasicInfo/property/code')
}

// 获取运营管理单位
export function operateCode () {
  return get('/zbkc/sysOrg/parentOrg')
}

// 获取运营单位项目
export function operateUnit (id) {
  return get('/zbkc/sysOrg/sonOrg/' + id)
}

// 新增大厦,栋，层，室
export function addBuild (data) {
  return post('/zbkc/wyBasicInfo/addBasic', data)
}

// 大厦,栋，层，室查看
export function viewBuild (id) {
  return get('/zbkc/wyBasicInfo/details/' + id)
}

// 修改 大厦,栋，层，室
export function editBuild (data) {
  return post('/zbkc/wyBasicInfo/update', data)
}

// 地址查询
export function searchAddress (obj) {
  return get('/zbkc/area/details/' + obj)
}

// 物业名称查询
export function propertyName (obj) {
  return get('/zbkc/wyBasicInfo/property/name/' + obj)
}

// 物业名称查询接口
export function assetName (obj) {
  return get('/zbkc/wyBasicInfo/property/nameCheck/' + obj)
}

// 删除
export function parkdelete (id) {
  return get('/zbkc//wyBasicInfo/del/' + id)
}
