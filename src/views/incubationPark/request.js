import {get} from '@/until/request.js'

// 获取标签
export function selectTag (obj) {
  return get('/zbkc/sysDataType/code/' + obj)
}

// 获取街道 南山区code(440305)
export function streetCode (obj) {
  return get('/zbkc/area/streetCode/' + obj)
}

// 获取社区
export function committeeCode (obj) {
  return get('/zbkc/area/committeeCode/' + obj)
}

// 删除
export function parkdelete (id) {
  return get('/zbkc//wyBasicInfo/del/' + id)
}
