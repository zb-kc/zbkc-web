import {get, post} from '@/until/request.js'
// 空置列表
export function emptyData (obj) {
  return get('/zbkc/wyBasicInfo/emptys' + obj)
}
// 使用列表
export function useData (obj) {
  return get('/zbkc/wyBasicInfo/useds' + obj)
}
// 产权列表
export function propertyData (obj) {
  return get('/zbkc/wyBasicInfo/prorights' + obj)
}

// 入账列表
export function entryData (obj) {
  return get('/zbkc/wyBasicInfo/entrys' + obj)
}

// 楼栋列表
export function buildData (obj) {
  return get('/zbkc/wyBasicInfo/building' + obj)
}

// 楼栋列表
export function floorData (obj) {
  return get('/zbkc/wyBasicInfo/building' + obj)
}

// 单元列表
export function roomData (obj) {
  return get('/zbkc/wyBasicInfo/building' + obj)
}

// 物业详情
export function wyDetails (id) {
  return get('/zbkc/wyBasicInfo/details/' + id)
}

export function allProRight () {
  return post('/zbkc/wyAssetEntry/allProRight?userId=' + sessionStorage.getItem('userId'))
}

export function listId (obj) {
  return get('/zbkc/yyUseReg/row/' + obj)
}

export function selectTag (obj) {
  return get('/zbkc/sysDataType/code/' + obj)
}
export function Listid (obj) {
  return get('/zbkc/wyProRightReg/row/' + obj)
}
export function addEntry (obj) {
  return post('/zbkc/wyProRightReg/addProRight', obj)
}
export function update (obj) {
  return post('/zbkc/wyProRightReg/upd', obj)
}
// 地址查询
export function searchAddress (obj) {
  return get('/zbkc/area/details/' + obj)
}
