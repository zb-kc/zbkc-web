const pagesGenerator = require('./plop-templates/views/prompt')
const componentsGenerator = require('./plop-templates/components/prompt')

module.exports = function (plop) {
    plop.setGenerator("views",pagesGenerator)
    plop.setGenerator("components",componentsGenerator)
};